#pragma once
#include "tstack.h"
#include"process.h"
#include <stdio.h>
#include <iostream>

class Tqueue:public TStack
{
protected:
	int Startpos;//������ ������� �������� �������
	int Lastpos;//������ ���������� �������� � �������
	int Queuecount;//������� ����� ����������� � ������� 

public:
	Tqueue(int Size = DefMemSize) :Startpos(0), Lastpos(-1), Queuecount(0) {} //����������� � ����������
	virtual void Put( TProcess &proc);//����� ���������� � �������
	virtual TData Get(void);//����� "�����" �������� �� ������� 
	virtual bool IsFull(void) const;//�������� �� �������	
	virtual bool IsEmpty(void) const;//�������� �� �������
	int GetQueueCount() { return Queuecount; }//����� ��� �������� ��� ����� � �������
	bool IsEnd() { return (Lastpos == DefMemSize-1); }//�������� �� ����� �������
	int GetStartIndex() { return Startpos; }//����� ������ ������ ������� �������� �������
	int GetLastIndex() { return Lastpos; }//����� ������ ������  ����������� �������� �������
	int GetIdProcOnStartPos(); //����� ������ ID �������� ������� �������
	int GetIdProcOnLastPos();//����� ������ ID �������� ���������� � �������




};