#include "Tqueue.h"
#include <stdio.h>
#include <iostream>

void Tqueue::Put( TProcess &proc) 
{
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else if (IsFull())
		SetRetCode(DataFull);
	else
	{	
		if(IsEnd() ) //��������� "������ ��������".��������� ����� ���������� ������ ��������� �������.
			Lastpos = Startpos-(DefMemSize-Queuecount);//e��� �� ����� �� ����� �������, � ��������� ����� ��������� � ������,�� "������ �� ����������"
		pMem[++Lastpos] = proc.GetProcId();
		Queuecount++;
	}
}

TData Tqueue::Get(void)
{

	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if (IsEmpty())
			SetRetCode(DataEmpty);
		else
		{
			Queuecount--;
			if (IsEmpty())
			{
				int temp = pMem[Startpos];
				Startpos = 0;
				return temp;
			}

			else
				return pMem[Startpos++];
		}

}

bool Tqueue::IsEmpty(void) const 
{
	return (Queuecount == 0);
}

bool Tqueue::IsFull(void) const
{
	return(Queuecount == DefMemSize);
}

int Tqueue::GetIdProcOnStartPos() 
{ 
	try
	{
		if (IsEmpty()) throw 1;
		else
			return pMem[Startpos];
	}
	catch (int err)
	{
		if (err == 1)
			cout << "Equeue is Free.Can't get proc ID on start pos" 
			     << endl << endl;
		throw 1;
	}
	
}

int Tqueue::GetIdProcOnLastPos()
{ 
	try
	{
		if (IsEmpty()) throw 1;
		else
			return pMem[Lastpos];
	}
	catch (int err)
	{
		if (err == 1)
			cout << "Equeue is Free.Can't get proc ID on last pos" 
			     << endl << endl;
		throw 1;
	}
	
}