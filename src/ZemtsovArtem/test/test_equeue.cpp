#include <gtest.h>
#include <process.h>
#include <TProc.h>
#include <Tqueue.h>

TEST(CPU,cpu_changed_available)
{
	CPU cpu;
	cpu.Use(8);
	EXPECT_EQ(false, cpu.IsFree() );
}


TEST(CPU, can_not_setfree_cpu_when_he_is_free)
{
	CPU cpu;
	
	ASSERT_ANY_THROW(cpu.SetFree() );
}


TEST(TProcess, we_can_right_get_procID_and_set_it_too)
{
	TProcess proc1(0), proc2(1);
	proc1.SetId(1);
	EXPECT_EQ(proc1.GetProcId(), proc2.GetProcId());
}

TEST(Tqueue, we_can_put_in_queue_in_hard_situation)//��������� �������� ����� �� ��������� ������� ���������, ����� ����� ������ �������(��� ����� ��������� ���� �����) � �������� �������� ��� �������
{
	Tqueue queue;
	TProcess temp1(1),temp2(2),temp3(3);
	for (int i = 0; i < DefMemSize; i++)
	{
		queue.Put(temp1);
	}
	queue.Get();
	queue.Put(temp2);
EXPECT_EQ(queue.GetIdProcOnStartPos() ,2 );
}

TEST(Tqueue, we_get_assert_when_we_try_to_get_proc_id_in_the_free_place)
{
	Tqueue queue;
	ASSERT_ANY_THROW(queue.GetIdProcOnStartPos());
}

TEST(Tqueue, check_IsFree_and_IsFull_work)
{
	Tqueue queue;
	TProcess proc(23);
	bool checkIsfreeWhenFree, checkIsfullWhenFree, checkIsfreeWhenFull, checkIsfullWhenFull;
	checkIsfreeWhenFree = queue.IsEmpty();
	checkIsfullWhenFree = queue.IsFull();
	for (int i = 0; i < DefMemSize; i++)
	{
		queue.Put(proc);
	}
	checkIsfreeWhenFull = queue.IsEmpty();
	checkIsfullWhenFull = queue.IsFull();
	EXPECT_EQ(true,(checkIsfreeWhenFree &&(!checkIsfreeWhenFull)&&(!checkIsfullWhenFree) && checkIsfullWhenFull) );
}






