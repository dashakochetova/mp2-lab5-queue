#include "tqueue.h"
#include "tjobstream.h"

class TProc
{
private:
	TJobStream Str;
 	TQueue Job;
	bool unlocked;
	unsigned takts;
	unsigned CountJobs, CountRefuing, CountDowntime;
	int InProc;
public:
	TProc(unsigned short,unsigned short,unsigned,unsigned);
	void Service(void);
	void PrintState(void);
	void PrintResult(void);
};