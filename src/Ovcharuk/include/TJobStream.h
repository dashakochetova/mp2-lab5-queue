#include <time.h>
#include <iostream>

const int max = 100;

class TJobStream {
private:
	int ID;
	int q1, q2;
public:
	TJobStream(int _q1, int _q2){
		ID = 0;
		q1 = _q1;
		q2 = _q2;
		srand(time(NULL));
	}

	int GetID(void){
		return ID;
	}

	bool isReady(void){
		return (rand() % max < q2) ? true : false;
	}

	int addJob(void){
		int result = 0;
		if (rand() % max < q1)
			result = ++ID;
		return result;
	}
};