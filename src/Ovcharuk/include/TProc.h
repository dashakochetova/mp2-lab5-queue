#include "tqueue.h"
#include "TJobStream.h"

class TProc{
private:
	TJobStream stream;
	TQueue memory;
	int tacts;
	bool ready = true;
	int CountJobs, CountFailure, CountDowntime;
public:
	TProc(int _tacts, int _size, int _q1, int _q2) :stream(_q1, _q2), memory(_size), tacts(_tacts)
	{
		CountJobs = 0;
		CountFailure = 0;
		CountDowntime = 0;
	}

	void startWork(void){
		for (int i = 0; i < tacts; i++)
		{
			if (stream.addJob())
				if (memory.isFull())
					CountFailure++;
				else
					memory.Put(stream.GetID());

			if (ready){
				if (memory.isEmpty())
					CountDowntime++;
				else
				{
					memory.Get();
					ready = false;
				}
			}

			if (!ready){
				ready = stream.isReady();
				if (ready)
					CountJobs++;
			}
		}
	}

	void Results(void){
		cout << endl;
		cout << "���������� ����������� �������: " << stream.GetID() << endl;
		cout << "���������� �������, ������� ������������ �����������: " << CountJobs << endl;
		cout << "���������� �������, �� ������������ �����������: " << stream.GetID() - CountJobs << endl;
		cout << "���������� ������� � ������������ ������� ��-�� ������������ �������: " << CountFailure << endl;
		cout << "������� �������: " << ((stream.GetID()) ? floor(CountFailure * 100 / stream.GetID()) : 0) << "%" << endl;
		cout << "C������ ���������� ������ ���������� �������: " << ((CountJobs) ? floor((tacts - CountDowntime) / CountJobs) : 0) << endl;
		cout << "���������� ������ �������: " << CountDowntime << endl;
		cout << "������� �������: " << ((tacts) ? floor(CountDowntime * 100 / tacts) : 0) << "%" << endl;
	}
};